@extends('layouts.dashcust')

@section('content')
     <!--section content-->
     <div class="section-content section-dasboard-home">
        <div class="container-fluid">
            <div class="dashboard-heading">
                <h2>Dashboard</h2>
                <p class="dashboard-subtitle">
                    Look what you have made today!
                </p>
            </div>
            <div class="dashboard-content">
                <div class="row mt-5">
                    <div class="col-md-4">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div
                                    class="dashboard-card-title"
                                >
                                    ORDER SUCCESS
                                </div>
                                <div
                                    class="
                                        dashboard-card-subtitle
                                    "
                                >
                                    15,209
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div
                                    class="dashboard-card-title"
                                >
                                    ORDER PENDING
                                </div>
                                <div
                                    class="
                                        dashboard-card-subtitle
                                    "
                                >
                                    15,209
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div
                                    class="dashboard-card-title"
                                >
                                    ORDER FAILED
                                </div>
                                <div
                                    class="
                                        dashboard-card-subtitle
                                    "
                                >
                                    15,209
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection