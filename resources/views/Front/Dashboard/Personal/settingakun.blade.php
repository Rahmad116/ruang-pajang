@extends('layouts.dashcust')

@section('content')
<div class="section-content section-dasboard-home">
    <div class="container-fluid">
        <div class="dashboard-heading">
            <h2>My Account</h2>
            <p class="dashboard-subtitle">
                Informasi Akun Anda!
            </p>
        </div>
        <div class="dashboard-content">
            <div class="row mt-1">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label
                                            for="name-company"
                                            >Nama
                                        </label>
                                        <input
                                            type="text"
                                            class="
                                                form-control
                                            "
                                            id="name-company"
                                            value="Hotel Makamur"
                                        />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email"
                                            >Email
                                        </label>
                                        <input
                                            type="text"
                                            class="
                                                form-control
                                            "
                                            id="email"
                                            value="Makamur@mail.com"
                                        />
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label for="phone"
                                            >Phone
                                        </label>
                                        <input
                                            type="text"
                                            class="
                                                form-control
                                            "
                                            id="phone"
                                            value="Hotel Makamur"
                                        />
                                    </div>
                                </div>

                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label for="alamat"
                                            >Alamat
                                        </label>
                                        <textarea
                                            name="alamat"
                                            id="alamat"
                                            class="
                                                form-control
                                            "
                                        >
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label for="provisi"
                                            >Provinsi
                                        </label>
                                        <select
                                            name="provinsi"
                                            id="provinsi"
                                            class="
                                                form-control
                                            "
                                        >
                                            <option
                                                value="Jawa Tengah"
                                            >
                                                Jawa Tengah
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5 mt-2">
                                    <div class="form-group">
                                        <label
                                            for="kabupaten"
                                            >Kabupaten
                                        </label>
                                        <select
                                            name="kabupaten"
                                            id="kabupaten"
                                            class="
                                                form-control
                                            "
                                        >
                                            <option
                                                value="Cilacap"
                                            >
                                                Cilacap
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-5 mt-2">
                                    <div class="form-group">
                                        <label
                                            for="kecamatan"
                                            >Kecamatan
                                        </label>
                                        <select
                                            name="kecamatan"
                                            id="kecamatan"
                                            class="
                                                form-control
                                            "
                                        >
                                            <option
                                                value="Condongatur"
                                            >
                                                Condongcatur
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 mt-2">
                                    <div class="form-group">
                                        <label for="kodepos"
                                            >Kode Pos
                                        </label>
                                        <input
                                            type="text"
                                            class="
                                                form-control
                                            "
                                            id="kode-pos"
                                            value="1200"
                                        />
                                    </div>
                                </div>

                                <div class="col-md-5 mt-2">
                                    <div class="form-group">
                                        <label
                                            for="username"
                                            >Password
                                        </label>
                                        <input
                                            type="password"
                                            class="
                                                form-control
                                            "
                                            id="password"
                                            value="123"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col text-right">
                                    <button
                                        type="submit"
                                        class="
                                            btn btn-success
                                            mt-3
                                        "
                                        style="float: right"
                                    >
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection