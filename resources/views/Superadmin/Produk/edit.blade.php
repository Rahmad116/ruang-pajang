@extends('layouts.stisla')
@section('css')
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
@endsection

@section('content')
<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-10">
              <div class="row">
                <div class="col-md-12 text-center">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ $error }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    @endforeach
                </div>
              </div>
          <div class="card">
            <div class="card-header">
                <h4>Edit Data Produk {{ $produk->judul }}</h4>
            </div>
            <div class="card-body">
             <form action="{{ route('produk.update', $produk) }}" method="POST" enctype="multipart/form-data">
                 @csrf
                 @method('PUT')
                  <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Kategori</label>
                    <select class="form-select form-control" name="kategori_id" aria-label="Default select example">
                        <option value="">-- Pilih Salah Satu --</option>
                        @forelse ($kategori as $item)
                            <option value="{{ $item->id }}" {{ ($item->id == $produk->kategori_id) ? 'selected' : '' }}>{{ $item->namakategori }}</option>
                        @empty
                            
                        @endforelse
                      </select>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Pelukis</label>
                    <select class="form-select form-control" name="pelukis_id" aria-label="Default select example">
                        <option value="">-- Pilih Salah Satu --</option>
                        @forelse ($pelukis as $item)
                            <option value="{{ $item->id }}" {{ ($item->id == $produk->pelukis_id) ? 'selected' : '' }}>{{ $item->nama }}</option>
                        @empty
                            
                        @endforelse
                      </select>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Status</label>
                    <select class="form-select form-control" name="status" aria-label="Default select example">
                        <option value="">-- Pilih Salah Satu --</option>
                        <option value="tersedia" {{ ($produk->status == 'tersedia') ? 'selected' : '' }}>-- Tersedia --</option>
                        <option value="tersewa" {{ ($produk->status == 'tersewa') ? 'selected' : '' }}>-- Tersewa --</option>
                        <option value="terjual" {{ ($produk->status == 'terjual') ? 'selected' : '' }}>-- Terjual --</option>
                        
                      </select>
                  </div>
                <div class="mb-3">
                    <label class="form-label">Judul</label>
                    <input type="text" class="form-control" name="judul" placeholder="judul lukisan" value="{{ old('judul', $produk->judul) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Aliran Lukis</label>
                    <input type="text" class="form-control" name="aliranlukis" placeholder="aliran lukis" value="{{ old('aliranlukis', $produk->aliranlukis) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Tahun Pembuatan</label>
                    <input type="number" class="form-control" name="tahunpembuatan" placeholder="tahun karya" value="{{ old('tahunpembuatan', $produk->tahunpembuatan) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Harga Sewa</label>
                    <input type="number" class="form-control" name="hargasewa" placeholder="harga sewa" value="{{ old('hargasewa', $produk->hargasewa) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Harga Jual</label>
                    <input type="number" class="form-control" name="hargajual" placeholder="harga jual" value="{{ old('hargajual', $produk->hargajual) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Harga Fix Jual</label>
                    <input type="number" class="form-control" name="hargafix" placeholder="harga fix jual" value="{{ old('hargafix', $produk->hargafix) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Dimensi</label>
                    <input type="text" class="form-control" name="dimensi" placeholder="dimensi" value="{{ old('dimensi', $produk->dimensi) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Media</label>
                    <input type="text" class="form-control" name="media" placeholder="media" value="{{ old('media', $produk->media) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Berat</label>
                    <input type="number" class="form-control" name="berat" placeholder="berat" value="{{ old('berat', $produk->berat) }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{ old('deskripsi', $produk->deskripsi) }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Foto</label>
                    <input class="form-control" type="file" name="foto" id="formFile">
                    <div class="form-group mt-2" style="max-width: 20rem;">
                        <img width="300" src="{{ asset($produk->foto) }}" alt="">
                      </div>
                </div>
                <div class="col-md-12 mb-5 text-center">
                    <button class="btn btn-primary">Simpan</button>
                </div>
             </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>
@endsection
