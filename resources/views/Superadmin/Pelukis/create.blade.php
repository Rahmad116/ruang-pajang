@extends('layouts.stisla')
@section('css')
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
@endsection

@section('content')
<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-10">
              <div class="row">
                <div class="col-md-12 text-center">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ $error }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    @endforeach
                </div>
              </div>
          <div class="card">
            <div class="card-header">
                <h4>Tambah Data Pelukis</h4>
            </div>
            <div class="card-body">
             <form action="{{ route('pelukis.store') }}" method="POST" enctype="multipart/form-data">
                 @csrf
                <div class="mb-3">
                    <label class="form-label">Nama Pelukis</label>
                    <input type="text" class="form-control" name="nama" placeholder="nama pelukis" value="{{ old('nama') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Aliran Lukis</label>
                    <input type="text" class="form-control" name="aliranlukis" placeholder="aliran lukis" value="{{ old('aliranlukis') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Tahun Karya</label>
                    <input type="number" class="form-control" name="tahunkarya" placeholder="tahun karya" value="{{ old('tahunkarya') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{ old('deskripsi') }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Foto</label>
                    <input class="form-control" type="file" name="foto" id="formFile">
                </div>
                <div class="col-md-12 mb-5 text-center">
                    <button class="btn btn-primary">Simpan</button>
                </div>
             </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>
@endsection
