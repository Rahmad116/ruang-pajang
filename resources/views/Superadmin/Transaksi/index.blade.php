@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <h4>Semua Data Transaski Beli</h4>
            </div>
            <div class="card-body">
                <table class="card-table table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">
                                No
                            </th>
                            <th scope="col">
                                Invoice
                            </th>
                            <th scope="col">
                                Tanggal
                                Pembelian
                            </th>
                            <th scope="col">
                                Total Bayar
                            </th>
                            <th scope="col">
                                Status
                            </th>
                            <th scope="col">
                                Cek Detail
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $item)
                            <tr>
                                <th scope="row">
                                    1
                                </th>
                                <td>{{ $item->invoice }}</td>
                                <td>{{ Carbon\Carbon::parse($item->tanggalreservasi)->isoFormat('D MMMM Y') }}</td>

                                <td>Rp.{{ number_format($item->totalharga) }}</td>
                                <td>
                                    <div class="status" style="color: red;">
                                        {{ $item->statuspembayaran }}
                                    </div>
                                </td>
                                <td>
                                    <p>
                                        <a class="btn btn-success" href="{{ route('konfirmasi.beli', $item->id) }}">
                                            Konfirmasi
                                        </a>
                                        <a class="btn btn-warning" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                          Detail
                                        </a>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <div class="collapse" id="collapseExample">
                                        <div class="card card-body">
                                            <table class="table text-center">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">Foto</th>
                                                    <th scope="col">Dimensi</th>
                                                    <th scope="col">Harga @</th>
                                                    <th scope="col">Seniman</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($item->detailbeli  as $row)
                                                        <tr>
                                                            <td>
                                                                <img src="{{ asset($row->foto) }}" class="img-fluid" width="150px" alt="">
                                                            </td>
                                                            <td>{{ $row->dimensi }}</td>
                                                            <td>Rp {{ number_format($row->harga) }}</td>
                                                            <td>{{ $row->seniman }}</td>
                                                        </tr>
                                                     
                                                    @empty
                                                        
                                                    @endforelse
                                                 
                                                </tbody>
                                              </table>
                                        </div>
                                      </div>
                                </td>
                                
                            </tr>
                        @empty
                            
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
