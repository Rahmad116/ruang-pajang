@extends('layouts.front')

@section('content')
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="{{ asset('front') }}/img/fotohome.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('front') }}/img/fotohome.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('front') }}/img/fotohome.png" class="d-block w-100" alt="...">
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
                <section class="kategori mt-3">
                    <h5>Tren Categories</h5>
                    <div class="row mt-4 justify-content-center">
                        <div class="col-md-12 slide-slick">
                            @forelse ($kategoris as $kategori)
                                <div class="img-kategori">
                                    <img src="{{ asset($kategori->foto) }}" alt="">
                                    <p>{{ $kategori->namakategori }}</p>
                                </div>
                            @empty
                                
                            @endforelse
                        
                            {{-- <div class="img-kategori">
                                <img src="{{ asset('front') }}/img/kategori1.png" alt="">
                                <p>Nature</p>
                            </div>
                            <div class="img-kategori">
                                <img src="{{ asset('front') }}/img/kategori2.png" alt="">
                                <p>Hewan</p>
                            </div>
                            <div class="img-kategori">
                                <img src="{{ asset('front') }}/img/kategori.png" alt="">
                                <p>Kriya</p>
                            </div>
                            <div class="img-kategori">
                                <img src="{{ asset('front') }}/img/kategori2.png" alt="">
                                <p>Patung</p>
                            </div> --}}
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12 text-center">
                            <p>
                                Enjoy  Buy and Rent Gallery <br> 
                                From Best Artists
                            </p>
                        </div>
                        <div class="col-md-12 text-center">
                            <a class="btn btn-primary" href="">View All Gallery</a>
                        </div>
                    </div>

                    <div class="row mt-4 mb-5 justify-content-center">
                        <div class="col-md-4 text-center">
                            <div class="card">
                                <div class="card-body">
                                    Our Colection <br>
                                    500
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="card">
                                <div class="card-body">
                                    Our Artists <br>
                                    200
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="card">
                                <div class="card-body">
                                    Our Client <br>
                                    100
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <hr>

            @include('layouts.include.footerfront')
                
            </div>
@endsection