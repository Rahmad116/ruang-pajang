<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('front') }}/img/logo.svg">

{{-- <link rel="shortcut icon" href="malas_ngoding.ico"> --}}

    
    <link rel="stylesheet" href="{{ asset('front') }}/css/style.css">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- slick -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
    <style>
        .aktif {
            background-color: white;
            border-radius: 5px;
        }
    </style>
    <title>Ruang Pajang</title>
  </head>
  <body>

    <div class="side-navbar d-flex justify-content-between flex-wrap flex-column active-nav" id="sidebar">
      <ul class="nav flex-column text-white w-100">
        <a href="/" class="nav-link h3 text-white my-2">
            <img src="{{ asset('front') }}/img/logo.svg" alt="">
        </a>
        <a style="text-decoration: none" href="{{ route('art.gallery') }}">
            <li class="nav-link {{ Request::is('artGallery') ? 'aktif' : '' }}">
                <span class="mx-2">Art Gallery</span>
            </li>
        </a>

        {{-- <li class="nav-link">
          <span class="mx-2">Art Rental</span>
        </li> --}}

        <a style="text-decoration: none" href="{{ route('our.artist') }}">
            <li class="nav-link {{ Request::is('ourArtist') ? 'aktif' : '' }}">
                <span class="mx-2">Our Artist</span>
            </li>
        </a>
        <a style="text-decoration: none" href="{{ route('listkeranjang') }}">
            <li class="nav-link {{ Request::is('ListKeranjang') ? 'aktif' : '' }}">
                <span class="mx-2">Keranjang</span>
            </li>
        </a>
        <li class="nav-link">
          <span class="mx-2">About US</span>
        </li>
        <li class="nav-link">
          <span class="mx-2">Contact US</span>
        </li>
        @if (auth()->user() != null)
            <a class="btn btn-primary" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt text-danger"></i> Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        @endif

        @if (auth()->user() == null)
            <a class="btn btn-primary" href="{{ route('login') }}">Login</a>
        @endif
           
      </ul>
          
    </div>
    
    <div class="p-1 my-container active-cont" id="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="btn border-0" id="menu-btn">
                    <i class="bx bx-menu"><img src="{{ asset('front') }}/img/menu.png" class="img-fluid" alt=""></i>
                </a>

                <a href="{{ route('dashboard.customer') }}" class="navbar-toggler btn btn-outline-info" type="button"  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <img src="https://img.icons8.com/ios/50/000000/test-account.png"/>
                </a>
                <!-- <button class="navbar-toggler btn btn-outline-info" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <img src="https://img.icons8.com/windows/32/000000/user.png"/>
                </button> -->
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  
                </ul>
                  @if (auth()->user() != null)
                        {{ auth()->user()->name }} &nbsp;
                  @endif
          
                  @if (auth()->user() == null)
                        Silahkan Login Dahulu &nbsp;
                  @endif
                <a href="{{ route('dashboard.customer') }}">
                    <img src="https://img.icons8.com/ios/50/000000/test-account.png"/>
                </a>
              </div>
            </div>
          </nav>
          @include('sweetalert::alert')
          @yield('content')
    </div>

    
    <!-- Optional JavaScript; choose one of the two! -->
    <script src="{{ asset('front') }}/js/scriptjs.js"></script>

    <!-- optional jquery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
    <!-- slick -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    @stack('js')
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>