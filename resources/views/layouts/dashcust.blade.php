
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="stylesheet" href="{{ asset('front/admincust') }}/css/customer-dashboard.css" />
        <!-- Bootstrap CSS -->
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
        />

        <!-- slick -->
        <link
            rel="stylesheet"
            type="text/css"
            href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
        />
        <link
            rel="stylesheet"
            type="text/css"
            href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"
        />
        <title>Dashboard Customer Ruang Pajang</title>
    </head>
    <body>
        <!--Dashboard-->
        <div class="page-dashboard">
            <div class="d-flex" id="wrapper">
                <!--Sidebar-->
                <div class="border-right" id="sidebar-wrapper">
                    <div class="sidebar-heading text-center">
                        <a href="{{ route('welcome') }}">
                            <img src="{{ asset('front/admincust') }}/img/logo.svg" alt="" class="my-4 px-4" />
                        </a>
                    </div>
                    <div class="list-group list-group-flush">
                        <a
                            href="{{ route('dashboard.customer') }}"
                            class="
                                list-group-item list-group-item-action
                                {{ Request::is('MyDashboardPersonal*') ? 'active' : '' }}
                            "
                        >
                            DASHBOARD
                        </a>
                       
                        <a
                            href="{{ route('transaksi.beli.p') }}"
                            class="list-group-item list-group-item-action {{ Request::is('MyTransaksiPersonal*') ? 'active' : '' }}"
                        >
                            TRANSAKSI BELI
                        </a>
                        
                        <a
                            href="{{ route('setting.akun.p') }}"
                            class="list-group-item list-group-item-action {{ Request::is('SettingAkun*') ? 'active' : '' }}"
                        >
                            SETTINGS
                        </a>
                        <a
                            href="{{ route('welcome') }}"
                            class="list-group-item list-group-item-action {{ Request::is('Dasboard*') ? 'active' : '' }}"
                        >
                            KEMBALI BELANJA
                        </a>
                        <a class="list-group-item list-group-item-action" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt text-danger"></i> LOGOUT
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
                <!--Page Content-->
                <div id="page-content-wrapper">
                   {{-- Navbar --}}
                    <nav
                        class="
                            navbar navbar-expand-lg navbar-light
                            fixed-top
                            navbar-store
                        "
                    >
                        <div class="container-fluid">
                            <button
                                class="btn btn-secondary d-md-none mr-auto mr-2"
                                id="menu-toggle"
                            >
                                &laquo; Menu
                            </button>
                            <button
                                class="navbar-toggler"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#navbarNavAltMarkup"
                                aria-controls="navbarNavAltMarkup"
                                aria-expanded="false"
                                aria-label="Toggle navigation"
                            >
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div
                                class="collapse navbar-collapse"
                                id="navbarNavAltMarkup"
                            >
                                <div class="navbar-nav ms-auto">
                                    <a class="nav-link" href="#">{{ auth()->user()->name }}</a>
                                    <a
                                        class="
                                            nav-link
                                            active
                                            d-none d-sm-block
                                        "
                                        aria-current="page"
                                        href="#"
                                        ><img src="{{ asset('front/admincust') }}/img/person.png" alt="" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </nav>
                    @yield('content')
                </div>
            </div>
        </div>

        <!-- Optional JavaScript; choose one of the two! -->
        <script src="{{ asset('front/admincust') }}/js/scriptjs.js"></script>

        <!-- optional jquery -->
        <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        ></script>

        <!-- slick -->
        <script
            type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
        ></script>
        <!-- Menu Toggle Script -->

        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"
        ></script>

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    </body>
</html>
