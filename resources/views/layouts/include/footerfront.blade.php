<section class="footer mt-5 mb-4">
    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <b>LEARN MORE</b> <br>
            Panduan Pembelian <br>
            Panduan Sewa <br>
            Panduan Pembayaran
        </div>
        <div class="col-md-4 text-center">
            <b>CONTACT US</b> <br>
            Office   : Jl.Garuda No 89. Pringgolayan Bantul. Yogyakarta <br>
            Email   : indi@indi.com <br>
            Phone : 0277 987 0988
        </div>
        <div class="col-md-4 text-center">
            <b>CUSTOMER SERVICE</b> <br>
            Phone : 0277 987 0988
        </div>
    </div>
</section>