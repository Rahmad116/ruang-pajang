<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'Front\frontController@index')->name('welcome');
Route::get('/artGallery', 'Front\frontController@artGalley')->name('art.gallery');
Route::get('/ourArtist', 'Front\frontController@ourArtist')->name('our.artist');
Route::get('/detailart/{id}', 'Front\frontController@detail')->name('art.detail');
Route::get('/artisdetail/{id}', 'Front\frontController@artisdetail')->name('artis.detail');

Route::post('/keranjangbeli', 'Front\frontController@keranjangbeli')->name('keranjangbeli');
Route::post('/postregister', 'Front\frontController@postregister')->name('post.register');
// Route::group(['middleware' => ['auth', 'CekRole:customer'], 'prefix' => 'Customer'], function () {
// 	Route::namespace('Front')->group(function () {
		
// 	});
// });

// Route::get('/registercompany', 'Auth\RegisterController@registercompany')->name('register.company');
// Route::get('/registerpersonal', 'Auth\RegisterController@registerpersonal')->name('register.personal');
// Route::post('postpersonal', 'Auth\RegisterController@postpersonal')->name('post.personal');
// Route::post('postcompany', 'Auth\RegisterController@postcompany')->name('post.company');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/province','Front\rajaOngkirController@get_province')->name('province');
Route::get('/kota/{id}','Front\rajaOngkirController@get_city')->name('city');
Route::get('/origin={city_origin}&destination={city_destination}&weight={weight}&courier={courier}','Front\rajaOngkirController@get_ongkir')->name('ongkir');


// Route::get('/cekkabupaten', 'Front\rajaOngkirController@index');



Route::group(['middleware' => ['auth', 'CekRole:customercompany,customerpersonal']], function () {
	Route::get('/MyDashboardPersonal', 'Front\customerController@index')->name('dashboard.customer');
	Route::get('/MyTransaksiPersonal', 'Front\customerController@tbelipersonal')->name('transaksi.beli.p');
	Route::get('/SettingAkun', 'Front\customerController@settingakunp')->name('setting.akun.p');

	// transaksi
	Route::get('/ListKeranjang', 'Front\transaksiController@listkeranjangbeli')->name('listkeranjang');
	Route::get('/hapuscart/{key}', 'Front\transaksiController@hapuscart')->name('hapus.cart');
	Route::post('/ProsesBeli', 'Front\transaksiController@prosesbeli')->name('proses.orderbeli');
});

Route::group(['middleware' => ['auth', 'CekRole:superadmin'], 'prefix' => 'Superadmin'], function () {
	Route::namespace('Superadmin')->group(function () {
		Route::resource('kategoriproduk', 'kategoriProdukController');
		Route::resource('produk', 'produkController');
		Route::resource('pelukis', 'pelukisController')->parameters(['pelukis' => 'pelukis',]);
		Route::resource('ongkir', 'ongkirController');
		Route::resource('voucher', 'voucherController');
		Route::get('TransaksiBeli', 'TransaksiBeli@index')->name('t.beli.s');
		Route::get('KonfirmasiBeli/{id}', 'TransaksiBeli@konfirmasiBeli')->name('konfirmasi.beli');
	});
});

// run artisan command
Route::get('/artisan', function () {
	Artisan::call('config:cache');
	Artisan::call('config:clear');
	Artisan::call('route:clear');
	Artisan::call('cache:clear');
	return 'OK';
 });