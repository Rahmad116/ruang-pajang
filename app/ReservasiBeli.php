<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservasiBeli extends Model
{
    protected $guarded = ['id'];

    public function detailbeli()
    {
        return $this->hasMany(ReservasiDetailBeli::class, 'reservasi_beli_id', 'id');
    }
}
