<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $fillable = [
        'kategori_id',
        'pelukis_id',
        'foto',
        'judul',
        'tahunpembuatan',
        'aliranlukis',
        'hargasewa',
        'hargajual',
        'hargafix',
        'dimensi',
        'media',
        'status',
        'deskripsi',
        'berat',
    ];

    public function kategori()
    {
        return $this->belongsTo(KategoriProduk::class);
    }

    public function pelukis()
    {
        return $this->belongsTo(Pelukis::class);
    }
}
