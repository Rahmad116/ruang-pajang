<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Produk;
use App\ReservasiBeli;
use Illuminate\Http\Request;

class TransaksiBeli extends Controller
{
    public function index()
    {
        $data = ReservasiBeli::with('detailbeli')->get();
        // dd($data);
        return view('Superadmin.Transaksi.index', compact('data'));
    }

    public function konfirmasiBeli($id)
    {
        $data = ReservasiBeli::with('detailbeli')->find($id);
        $data->update([
            'statuspembayaran' => 'Terbayar'
        ]);
        foreach($data['detailbeli'] as $p)
        {
            $produk = Produk::find($p->id);
            $produk->update([
                'status' => 'terjual',
            ]);
        }
        toast('Data Beli Berhasil Dikonfirmasi','success');
        return redirect()->back();
    }
}
