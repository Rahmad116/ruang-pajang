<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\TransaksiBeli;
use App\Produk;
use App\ReservasiBeli;
use App\ReservasiDetailBeli;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class transaksiController extends Controller
{
    
    public function listkeranjangbeli()
    {
        $carts = json_decode(request()->cookie('rp-carts'), true);
        if ($carts == null) {
            toast('Keranjang Masih Kosong', 'info');
            return redirect()->route('welcome');
        }
        // dd($carts);
        foreach ($carts as $key => $item) {
            $produk = Produk::find($item['produk_id']);
            if ($produk['status'] == 'terjual') {
                unset($carts[$key]);
                $cookie = cookie('rp-carts', json_encode($carts), 2880);
                alert()->info('Produk Yang Dihapus Sudah Terjual','Pilih Produk Lainnya');
                return redirect()->route('listkeranjang')->cookie($cookie);
            }
        }
        
        $provinsi = app('App\Http\Controllers\Front\rajaOngkirController')->get_province();
        // dd($provinsi);
        $carts = json_decode(request()->cookie('rp-carts'), true);
        // if ($carts == null) {
        //     toast('Keranjang Masih Kosong', 'info');
        //     return redirect()->route('welcome');
        // }
        $id = auth()->user()->id;
        $userdetail = UserDetail::where('user_id', $id)->first();
        // dd($userdetail);
        // dd($carts);
        // total berat
        $totalberat = 0;
        foreach($carts as $r){
            $totalberat += $r['berat'];
        }
        $berat = $totalberat * 1000;

        // total price
        $totalprice = 0;
        foreach($carts as $r){
            $totalprice += $r['price'];
        }
        $jml = count($carts);
        // dd(Str::random(15));

        return view('Front.keranjangbeli', compact('carts', 'userdetail', 'provinsi', 'berat', 'totalprice', 'jml'));
    }

    public function hapuscart($key)
    {
        $carts = json_decode(request()->cookie('rp-carts'), true);
        unset($carts[$key]);
        // dd($carts);
        $cookie = cookie('rp-carts', json_encode($carts), 2880);
        return redirect()->back()->cookie($cookie);
    }

    public function prosesbeli(TransaksiBeli $request)
    {

        // cek apakah sudah terjual produk
        
        DB::beginTransaction();
        try {

            $reservasibeli = ReservasiBeli::create([
                'user_id' => auth()->user()->id,
                'invoice' => Str::random(15),
                'tanggalreservasi' => date('Y-m-d H:i:s'),
                'alamatasal' => 'bantul',
                'alamatkirim' => $request->alamatkirim,
                'kurir' => $request->kurir,
                'ongkoskirim' => $request->ongkoskirim,
                'totalharga' => $request->totalharga,
                'totalqty' => $request->totalqty,
                'totalberat' => $request->totalberat,
                'statuspembayaran' => 'Menunggu Pembayaran',
                'statuspengiriman' => 'Diproses',
                'provinsi' => $request->provinsi,
                'namakota' => $request->namakota,
                'prov_id' => $request->province_id,
                'kota_id' => $request->kota_id,
            ]);
    
            $carts = json_decode(request()->cookie('rp-carts'), true);
    
            foreach ($carts as $row) {
                ReservasiDetailBeli::create([
                    'reservasi_beli_id' => $reservasibeli->id,
                    'produk_id' => $row['produk_id'],
                    'harga' => $row['price'],
                    'foto' => $row['foto'],
                    'dimensi' => $row['dimensi'],
                    'seniman' => $row['pelukis'],
                ]);
            }
            
            DB::commit();
    
            $carts = [];
            //KOSONGKAN DATA KERANJANG DI COOKIE
            $cookie = cookie('rp-carts', json_encode($carts), 2880);
            //REDIRECT KE HALAMAN FINISH TRANSAKSI
            return redirect()->back()->cookie($cookie);
        } catch (\Exception $e) {
            //JIKA TERJADI ERROR, MAKA ROLLBACK DATANYA
            DB::rollback();
            //DAN KEMBALI KE FORM TRANSAKSI SERTA MENAMPILKAN ERROR
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }
}
