<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\ReservasiBeli;
use Facade\FlareClient\View;
use Illuminate\Http\Request;

class customerController extends Controller
{
    public function index()
    {
        return view('Front.Dashboard.Personal.index');
    }

    public function tbelipersonal()
    {
        $data = ReservasiBeli::with('detailbeli')->get();
        // dd($data->detailbeli);
        return view('Front.Dashboard.Personal.transaksibeli', compact('data'));
    }

    public function settingakunp()
    {
        return view('Front.Dashboard.Personal.settingakun');
    }
}
