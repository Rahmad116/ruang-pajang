<?php

namespace App\Http\Requests\Superadmin\Pelukis;

use Illuminate\Foundation\Http\FormRequest;

class pelukisUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'foto' => 'nullable|mimes:jpeg,png,jpg',
            'tahunkarya' => 'required',
            'aliranlukis' => 'required',
            'deskripsi' => 'required',
        ];
    }
}
