<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;

class TransaksiBeli extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "namapembeli" => "required",
            "nohp" => "required",
            "province_id" => "required",
            "kota_id" => "required",
            "kurir" => "required",
            "layanan" => "required",
            "totalharga" => "required",
            "ongkoskirim" => "required",
            "provinsi" => "required",
            "namakota" => "required",
            "totalberat" => "required",
            "totalqty" => "required",
        ];
    }
}
