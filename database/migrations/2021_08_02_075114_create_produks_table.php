<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pelukis_id');
            $table->foreignId('kategori_id');
            $table->string('foto');
            $table->string('judul');
            $table->string('tahunpembuatan');
            $table->string('aliranlukis');
            $table->integer('hargasewa');
            $table->integer('hargajual');
            $table->integer('hargafix');
            $table->string('dimensi');
            $table->string('media');
            $table->enum('status', ['tersedia', 'tersewa', 'terjual']);
            $table->text('deskripsi');
            $table->integer('berat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
}
