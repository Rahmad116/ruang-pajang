<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProvinCityOnUserdetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->string('provinsi')->nullable();
            $table->string('prov_id')->nullable();
            $table->string('kabupaten')->nullable();
            $table->string('kab_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->dropColumn('provinsi');
            $table->dropColumn('prov_id');
            $table->dropColumn('kabupaten');
            $table->dropColumn('kab_id');
        });
    }
}
